import random

import pygame
from pygame import Vector2
from sir.epidemie import paramEpidemie

import core

colorFamily = {"sain": (255, 255, 255), "infecté": (255, 0, 0), "retabli": (0, 255, 0)}


class Body:
    def __init__(self, parent):
        self.parent = parent
        self.position = Vector2(random.randint(0, 800), random.randint(0, 600))
        self.velocity = Vector2(random.uniform(-5, 5), random.uniform(-5, 5))
        self.acceleration = Vector2()
        self.maxAcceleration = 1
        self.maxSpeed = 6
        self.taille = 5

        if self.parent.statut == "sain":
            self.couleur = colorFamily["sain"]
        elif self.parent.statut == "infecté":
            self.couleur = colorFamily["infecté"]
        elif self.parent.statut == "remis":
            self.couleur = colorFamily["remis"]

    def repulsion(self, obstacle):
        obstacleVect = Vector2(obstacle[0], obstacle[1])
        self.acceleration = self.position - obstacleVect

    def attraction(self, obstacle):
        obstacleVect = Vector2(obstacle[0], obstacle[1])
        self.acceleration = obstacleVect - self.position

    def deplacementAleatoire(self):
        self.acceleration = pygame.Vector2(random.uniform(-1, 1), random.uniform(-1, 1))
        if self.acceleration.length() > self.maxAcceleration:
            self.acceleration.scale_to_length(self.maxAcceleration)

        self.velocity = self.velocity + self.acceleration

        if self.velocity.length() > self.maxSpeed:
            self.velocity.scale_to_length(self.maxSpeed)

        self.position = self.position + self.velocity

        self.acceleration = pygame.Vector2(0, 0)

    def move(self):
        pass

    def bordure(self, fenetre):
        if self.position.y < 0:
            self.position.y = fenetre[1]

        if self.position.y > fenetre[1]:
            self.position.y = 0

        if self.position.x < 0:
            self.position.x = fenetre[0]

        if self.position.x > fenetre[0]:
            self.position.x = 0

    def show(self):
        core.Draw.circle(self.couleur, self.position, self.taille)

    def flipCoin(p):
        r = random.random()
        return r < p

    def update(self):
        for i in self.parent.perception:
            if i.statut == "infecté":
                if i.body.position.distance_to(self.position) < paramEpidemie["distMinContagion"]:
                    if self.flipCoin(paramEpidemie["probContagion"]):
                        self.parent.statut = "infecté"
                        self.couleur = colorFamily["infecté"]


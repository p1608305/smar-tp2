import pygame
import core
from sir.agent import Agent

Statuts = {"sain", "infecté", "retabli"}
colorFamily = {"sain": (255, 255, 255), "infecté": (255, 0, 0), "retabli": (0, 255, 0)}

def addAgent():
    pass


def supprAgent(agar):
    pass


def getAgent():
    pass


def computePerception():
    tabObjets = []

    for a in core.memory("agents"):
        a.perception = []
        for o in core.memory("agents"):
            if a.fustrum.inside(o):
                a.perception.append(o)


def computeDecision():
    decision = dict()
    distanceObstacle = 50

    for a in core.memory("agents"):
        obstacle = None
        for o in a.perception:
            if o.statut == "infecté":
                if o.pos.distance_to(a.body.pos) < distanceCible:
                    obstacle = o
                    distanceCible = o.pos.distance_to(a.body.pos)
            elif o.statut == "sain":
                pass
            elif o.statut == "retabli":
                pass
        decision.update({a: obstacle})

    return decision


def applyDecision(decision):
    for a in core.memory("agents"):
        if decision[a] is not None:
            print("#######################################################")
            a.body.repulsion(decision[a].body.pos)
        else:
            a.body.deplacementAleatoire()
        a.body.update()
        a.body.bordure(core.WINDOW_SIZE)


def setup():
    core.fps = 30
    core.WINDOW_SIZE = [800, 600]

    core.memory("nbAgents", 100)
    core.memory("agents", [])

    for a in range(0, core.memory("nbAgents")):
        core.memory("agents").append(Agent())


def run():
    core.cleanScreen()

    # CONTROL
    if core.getMouseLeftClick():
        mousePos = pygame.mouse.get_pos()
        distAgent = 10000
        infecte = None
        for a in core.memory("agents"):
            if a.body.position.distance_to(mousePos) < distAgent:
                infecte = a
                distAgent = a.body.position.distance_to(mousePos)
        infecte.statut = "infecté"
        infecte.body.couleur = colorFamily["infecté"]

    # AFFICHAGE
    for a in core.memory("agents"):
        a.body.show()

    # MISE À JOUR DES POSITIONS
    computePerception()
    decision = computeDecision()
    applyDecision(decision)


core.main(setup, run)

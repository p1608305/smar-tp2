from sir.body import Body
from sir.fustrum import Fustrum

# Statuts = {"sain", "infecté", "retabli"}


class Agent:
    def __init__(self, statut="sain"):
        self.fustrum = Fustrum(self)
        self.perception = []
        self.statut = statut
        self.body = Body(self)
